package org.organizi.activityservice.repositories;

import org.organizi.activityservice.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ActivityRepository extends JpaRepository<Activity,Long> {
}
