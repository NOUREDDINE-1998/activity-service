package org.organizi.activityservice.controller;

import org.organizi.activityservice.entities.Activity;
import org.organizi.activityservice.services.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
//@RequestMapping("/api")
public class ActivityController {
    @Autowired
    ActivityService activityService;

    @GetMapping("/activities")
    private ResponseEntity<List<Activity>> getAllActivities()
    {
        try {
            List<Activity> activities = new ArrayList<Activity>();
            activityService.getAllActivities().forEach(activities::add);
            if (activities.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(activities, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/activities/{id}")
    private ResponseEntity<Activity> getActivity(@PathVariable("id") long id)
    {
        Optional<Activity> activityData = Optional.ofNullable(activityService.getActivityById(id));
        if (activityData.isPresent()) {
            return new ResponseEntity<>(activityData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/SaveActivity")
    private ResponseEntity<Activity> saveActivity(@RequestBody Activity activity)
    {
        try {
            Activity activity1 = activityService.saveOrUpdate(activity);
            return new ResponseEntity<>(activity1, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/DeleteActivity/{id}")
    private ResponseEntity<HttpStatus>deleteActivity(@PathVariable("id") long id)
    {
        try {
            activityService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PutMapping("/EditActivity/{id}")
    private ResponseEntity<Activity> update(@RequestBody Activity activity,@PathVariable("id") long id)
    {
        Optional<Activity> activityData = Optional.ofNullable(activityService.getActivityById(id));
        if (activityData.isPresent()) {
            Activity activity1 = activityData.get();
            activity1.setName(activity.getName());
            activity1.setDescription(activity.getDescription());
            activity1.setDate(activity.getDate());
            activity1.setPlace(activity.getPlace());
            return new ResponseEntity<>(activityService.saveOrUpdate(activity1), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
