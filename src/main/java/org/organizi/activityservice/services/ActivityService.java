package org.organizi.activityservice.services;

import org.organizi.activityservice.entities.Activity;
import org.organizi.activityservice.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityService {

@Autowired
    ActivityRepository activityRepository;

    public List<Activity> getAllActivities()
    {
        List<Activity> activities = new ArrayList<>();
        activityRepository.findAll().forEach(activity -> activities.add(activity));
        return activities;
    }

    public Activity getActivityById(long id)
    {
        return activityRepository.findById(id).get();
    }

    public Activity saveOrUpdate(Activity activity)
    {
       return activityRepository.save(activity);
    }

    public void delete(long id)
    {
        activityRepository.deleteById(id);
    }

    public void update(Activity activity, long activityId)
    {
        activityRepository.save(activity);
    }
}
